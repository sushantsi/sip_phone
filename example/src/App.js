import React, { useState } from 'react'

import { ReactSipPhone, phoneStore } from 'iuehadnjfhasdiuf-hishdiug'
import 'iuehadnjfhasdiuf-hishdiug/dist/index.css'

const urlParams = new URLSearchParams(window.location.search)
//  const sipuri = 'webrtc_PpafDMxaMuu7@401auto.sip.newtws.com';
//   const password = 'PPovJLibINiTP7owRX';
//   const websocket = 'wss://api.phonecentral.co:5050';
//   const name = '401 Auto CRM';
//   const disabledButtons = [];
//   const disabledFeatures = ['settings'];
//   const outsideComponentDial = '';
//   const mode = '';
// const sipuri = 'webrtc_PpafDMxaMuu7@401auto.sip.newtws.com';
// const password = 'PPovJLibINiTP7owRX';

const sipuri = 'webrtc_b61Z0Gv3nrQl@401auto.sip.newtws.com'
const password = '7PIC6toObHOzYDM9de'

// const sipuri = 'webrtc_YTvw4GDjs93K@401auto.sip.newtws.com'
// const password = 'sUEEAZvFScaOTUkVkE'

const websocket = 'wss://api.phonecentral.co:5050'
const name = '401 Auto CRM'
const disabledButtons = ''
const disabledFeatures = []
const outsideComponentDial = 'true'
const mode = ''
//example url
//http://localhost:3000/phone/react-sip-phone?name=testname&websocket=wss://test-websocket-01-us-east-5.test.com:5065
//&sipuri=user_test@test.domain.com&password=tEsTpAsSwOrD&features=callbuttonsettings&buttons=holdtransfermute
const App = () => {
  const [dialstring, setDialstring] = useState('')

  const callbackChecking = () => {
    console.log('callback on child')
  }
  const incomingCallRedirect = (data) => {
    console.log('incoming call Redirection', data)
    console.log('name = ', data.name)
    console.log('ext = ', data.ext)
  }
  const missedCallRedirect = (data) => {
    console.log('missedCallRedirect call Redirection', data)
    console.log('name = ', data.name)
    console.log('ext = ', data.ext)
  }
  const handleDeclineCall = () => {
    console.log('call ended')
  }
  return (
    <React.Fragment>
      {outsideComponentDial ? (
        <div>
          <input
            placeholder='Enter a number to dial'
            onChange={(e) => setDialstring(e.target.value)}
          />
          <button
            onClick={() => {
              if (phoneStore.getState().sipAccounts.sipAccount && dialstring) {
                phoneStore
                  .getState()
                  .sipAccounts.sipAccount.makeCall(dialstring)
              }
            }}
          >
            Dial
          </button>
        </div>
      ) : null}
      <ReactSipPhone
        name={name || ''}
        sipCredentials={{
          sipuri: sipuri || '',
          password: password || ''
        }}
        sipConfig={{
          websocket: websocket || '',
          defaultCountryCode: '1'
        }}
        phoneConfig={{
          disabledButtons: disabledButtons || '', // Will remove button(s) from Phone component. E.g. hold transfer dialpadopen mute '
          disabledFeatures: disabledFeatures || '', // Will remove feature(s) from application. E.g. settings remoteid
          defaultDial: '', // (strict-mode only) the default destination. E.g. 1234567890
          sessionsLimit: 1, // limits amount of sessions user can have active
          attendedTransferLimit: 1, // limits amount of attendedTransfer sessions user can have active
          autoAnswer: false, // enable the auto-answer on incoming calls
          displayName: 'Sushant',
          moveToFunction: callbackChecking,
          handleIncoming: incomingCallRedirect,
          handleDeclineCallback: handleDeclineCall,
          handleMissedCallEvent: missedCallRedirect
        }}
        appConfig={{
          mode: mode || '', // 'strict' will activate a simple and limited user experience. set to sessionLimit 1 if using 'strict'
          started: false, // (strict-mode only) keeps track of call button visability during strict-mode
          appSize: 'large' // assign 'large' for larger font in status-name and session-status (not session remote-id/display name)
        }}
        width={0}
      />
    </React.Fragment>
  )
}

export default App
