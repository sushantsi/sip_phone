import * as React from 'react'

import { Session, SessionState, UserAgent } from 'sip.js'

import AttendedTransfer from './AttendedTransfer'
import BlindTranfer from './BlindTransfer'
import Dialpad from './Dialpad'
// import Hold from './Hold'
import Mute from './Mute'
import { PhoneConfig } from '../../models'
import { callDisconnect } from '../../util/TonePlayer'
import { connect } from 'react-redux'
import { endCall } from '../../actions/sipSessions'
import { getDurationDisplay } from '../../util/sessions'
import { setAppConfigStarted } from '../../actions/config'
import styles from './Phone.scss'
import toneManager from '../../util/ToneManager'

const endCallIcon = require('./assets/call_end-24px.svg')
// const dialpadIcon = require('./assets/dialpad-24px.svg')
// const transferIcon = require('./assets/arrow_forward-24px.svg')
const defaultUser = require('./assets/default-user-avatar.png')

interface Props {
  session: Session
  userAgent: UserAgent
  endCall: Function
  setAppConfigStarted: Function
  phoneConfig: PhoneConfig
  deviceId: string
  strictMode: string
  appSize: string
  // inputs: any
  // outputs:any
}

class Phone extends React.Component<Props> {
  state = {
    dialpadOpen: false,
    transferMenu: false,
    ended: false,
    transferDialString: '',
    attendedTransferStarted: false,
    duration: 0,
    counterStarted: false
  }

  constructor(props: any) {
    super(props)
    this.attendedProcess = this.attendedProcess.bind(this)
    console.log('this.props', this.props)
  }
  componentDidMount() {
    if (this.props.phoneConfig.disabledButtons.includes('dialpadopen')) {
      this.setState({ dialpadOpen: true })
    }
  }

  componentDidUpdate(newProps: Props) {
    console.log('newProps.session, ', newProps.session)
    if (
      newProps.session.state === SessionState.Established &&
      !this.state.counterStarted
    ) {
      this.handleCounter()
    } else {
      setTimeout(() => {
        console.log('its missed call')
      }, 5000)
    }
    if (
      newProps.session.state === SessionState.Terminated &&
      this.state.ended === false
    ) {
      this.setState({ ended: true })
      this.props.phoneConfig.moveToFunction()
    }
  }

  endCall() {
    if (this.props.session.state === SessionState.Established) {
      this.props.session.bye()
    } else if (
      this.props.session.state === SessionState.Initial ||
      this.props.session.state === SessionState.Establishing
    ) {
      toneManager.stopAll()
      callDisconnect(this.props.deviceId)
      // @ts-ignore
      this.props.session.cancel()
    }
    this.setState({ ended: true })
    setTimeout(() => {
      this.props.session.dispose()
      this.props.endCall(this.props.session.id)
      if (this.props.strictMode === 'strict') {
        this.props.setAppConfigStarted()
      }
    }, 5000)
    this.props.phoneConfig.moveToFunction()
  }

  attendedProcess(bool: boolean) {
    this.setState({ attendedTransferStarted: bool })
  }

  handleCounter() {
    if (
      this.props.session &&
      this.props.session.state !== SessionState.Terminated
    ) {
      if (this.state.counterStarted === false) {
        this.setState({ counterStarted: true })
      }
      setTimeout(() => {
        this.setState({ duration: this.state.duration + 1 })
        this.handleCounter()
      }, 1000)
    }
  }

  render() {
    const { state, props } = this
    let durationDisplay
    if (props.appSize === 'large') {
      if (
        this.props.session.state === SessionState.Initial ||
        this.props.session.state === SessionState.Establishing
      ) {
        durationDisplay = null
      } else {
        durationDisplay = (
          <div className={styles.statusLarge}>
            {getDurationDisplay(this.state.duration)}
          </div>
        )
      }
    } else {
      if (
        this.props.session.state === SessionState.Initial ||
        this.props.session.state === SessionState.Establishing
      ) {
        durationDisplay = null
      } else {
        durationDisplay = <div>{getDurationDisplay(this.state.duration)}</div>
      }
    }
    return (
      <React.Fragment>
        {/* <hr style={{ width: '100%' }} /> */}
        {/* <textarea  rows={15} cols={30} defaultValue={JSON.stringify(props.inputs)} />
        <textarea  rows={15} cols={30} defaultValue={JSON.stringify(props.outputs)} /> */}

        {/* {props.appSize === 'large' ? (
          <div className={styles.statusLarge} style={{color:"white"}}>
            {`${statusMask(props.session.state)}`}
          </div>
        ) : (
          <div style={{color:"white"}}>{`${statusMask(props.session.state)}`}</div>
        )} */}
        {props.phoneConfig.disabledFeatures.includes('remoteid') ? null : (
          <div
            className={styles.statusLarge}
            style={{ marginTop: '15%', color: 'white' }}
          >
            <img src={defaultUser} className='incomingDefaultUser' />
            <div id={styles.newincoming}>
              <p>{this.props.phoneConfig.displayName}</p>
              <div style={{ color: 'white', fontSize: 14 }}>
                {
                  // @ts-ignore
                  durationDisplay
                  //`Mobile ${props.session.remoteIdentity.uri.normal.user}  ${props.session.remoteIdentity._displayName}`
                }
              </div>
            </div>
          </div>
        )}

        <br />
        {/* {durationDisplay} */}
        {state.ended ? null : (
          <React.Fragment>
            <Dialpad open={state.dialpadOpen} session={props.session} />
            <div className={styles.actionsContainer}>
              {/* {props.phoneConfig.disabledButtons.includes('transfer') ? null : (
                <div id={styles.newincoming}>
                  <div
                    id={styles.actionButton}
                    // className={state.transferMenu ? styles.on : ''}

                    onClick={() =>
                      this.setState({ transferMenu: !state.transferMenu })
                    }
                  >
                    <img src={transferIcon} className='transferIcon' />
                  </div>
                  <p>Transfer</p>
                </div>
              )} */}
              {props.phoneConfig.disabledButtons.includes('mute') ? null : (
                <Mute session={props.session} />
              )}
              {/* {props.phoneConfig.disabledButtons.includes('numpad') ? null : (
                <div id={styles.newincoming}>
                  <div
                    id={styles.actionButton}
                    className={state.dialpadOpen ? styles.on : ''}
                    onClick={() =>
                      this.setState({ dialpadOpen: !state.dialpadOpen })
                    }
                  >
                    <img src={dialpadIcon} className='dialpadIcon' />
                  </div>
                  <p>Keypad</p>
                </div>
              )} */}

              {/* {props.phoneConfig.disabledButtons.includes('hold') ? null : (
                <Hold session={props.session} />
              )} */}

              <div
                id={styles.transferMenu}
                className={state.transferMenu ? '' : styles.closed}
              >
                <input
                  id={styles.transferInput}
                  onChange={(e) =>
                    this.setState({ transferDialString: e.target.value })
                  }
                  placeholder='Enter the transfer destination...'
                />
                {this.state.attendedTransferStarted ? null : (
                  <BlindTranfer
                    destination={state.transferDialString}
                    session={props.session}
                  />
                )}
                <AttendedTransfer
                  started={this.attendedProcess}
                  destination={state.transferDialString}
                  session={props.session}
                />
              </div>
            </div>
            <div style={{ textAlign: 'center', paddingTop: '80px' }}>
              <button
                className={styles.endCallButton}
                disabled={state.ended}
                onClick={() => this.endCall()}
              >
                <img src={endCallIcon} className='endCallIcon' />
              </button>
            </div>
            <audio id={this.props.session.id} autoPlay />
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}
const mapStateToProps = (state: any) => ({
  //inputs: state.device.audioInput,
  //outputs: state.device.audioOutput,
  stateChanged: state.sipSessions.stateChanged,
  sessions: state.sipSessions.sessions,
  userAgent: state.sipAccounts.userAgent,
  deviceId: state.device.primaryAudioOutput,
  strictMode: state.config.appConfig.mode,
  appSize: state.config.appConfig.appSize
})
const actions = {
  endCall,
  setAppConfigStarted
}
export default connect(mapStateToProps, actions)(Phone)
